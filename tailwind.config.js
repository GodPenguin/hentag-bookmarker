/* eslint-disable */
module.exports = {
  future: {},
  variants: {},
  plugins: [],
  content: [
    './src/**/*.html',
    './src/**/*.jsx',
    './src/**/*.js',
    './src/**/*.tsx',
    './src/**/*.ts'
  ],
  prefix: "hentag-"
};
