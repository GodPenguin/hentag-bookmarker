/* eslint-disable */
const common = require("./webpack.common.js");
const {merge} = require("webpack-merge");
const {DefinePlugin} = require("webpack");

module.exports = merge(common, {
  mode: "production",
  plugins: [
    new DefinePlugin({
      __HENTAG_URL__: JSON.stringify("https://hentag.com")
    })
  ]
});