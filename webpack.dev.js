/* eslint-disable */
const common = require("./webpack.common.js");
const {merge} = require("webpack-merge");
const {DefinePlugin} = require("webpack");

module.exports = merge(common, {
  mode: "production", // react dev version tries to inject something that cause issues
  devtool: "inline-source-map",
  plugins: [
    new DefinePlugin({
      __HENTAG_URL__: JSON.stringify("http://127.0.0.1:8080")
    })
  ]
});