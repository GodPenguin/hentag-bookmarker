# Hentag Bookmarker

The latest version can be found here: [hentag-bookmarker.user.js](https://gitlab.com/GodPenguin/hentag-bookmarker/-/raw/master/latest/hentag-bookmarker.user.js)

## Build Instructions
1. Install the latest LTS of `npm` and `node`
1. Run `npm install` to install dependencies
1. Run `npm run build:prod`
1. Build output will be at `dist/hentag-bookmarker.user.js`

## FAQ
### Why is the script I install all weird looking and not human-readable?
There are a few reasons for this:
1. When we release a script, we minify the code to make the script as small as possible.
   You can read more about code minification here:
   https://en.wikipedia.org/wiki/Minification_(programming)
1. Library dependencies are packaged alongside the source code so no additional network calls are needed when loading the script. This results in additional code being
   added that is not present in the source code. See `package.json` to see what libraries we depend on.




### Can this be made into a Chrome/Firefox extension instead?
Sadly no. Mozilla claims it goes against their "Acceptable Use Policy". We assume Google will do the same.