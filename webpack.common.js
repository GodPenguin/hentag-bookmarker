/* eslint-disable */
// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const path = require("path");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const webpack = require('webpack');
const TerserPlugin = require("terser-webpack-plugin");

const VERSION = "1.0.6"
const isProd = process.env.NODE_ENV === "production";
const suffix = isProd ? "" : "-dev"
const name = isProd ? "" : " Dev"

const userScriptMetadata = `// ==UserScript==
// @name        Hentag Bookmarker${name}
// @namespace   https://gitlab.com/GodPenguin/hentag-bookmarker
// @version     ${VERSION}${suffix}
// @author      GodPenguin
// @description Quickly bookmark pages to your Hentag account with the press of a button.
// @icon        https://hentag.com/static/icon/favicon-32x32.png
// @downloadURL ${isProd ? "https://gitlab.com/GodPenguin/hentag-bookmarker/-/raw/master/latest/hentag-bookmarker.user.js" : ""}
// @supportURL  https://gitlab.com/GodPenguin/hentag-bookmarker
// @homepageURL https://gitlab.com/GodPenguin/hentag-bookmarker
// @grant       GM.xmlHttpRequest
// @grant       GM.getValue
// @grant       GM.setValue
// @noframes
// @match       *://*.box.load.la/*
// @match       *://*.caitlin.top/*
// @match       *://*.comics.8muses.com/*
// @match       *://*.doujin.io/*
// @match       *://*.doujins.com/*
// @match       *://*.e-hentai.org/*
// @match       *://*.exhentai.org/*
// @match       *://*.fakku.net/*
// @match       *://*.hanime.tv/*
// @match       *://*.hentai2read.com/*
// @match       *://*.hentaifox.com/*
// @match       *://*.hentaigasm.com/*
// @match       *://*.hentaihaven.xxx/*
// @match       *://*.hentainexus.com/*
// @match       *://*.hitomi.la/*
// @match       *://*.hoshino.one/*
// @match       *://*.imhentai.xxx/*
// @match       *://*.irodoricomics.com/*
// @match       *://*.koharu.to/*
// @match       *://*.luscious.net/*
// @match       *://*.mangadex.org/*
// @match       *://*.nhentai.com/*
// @match       *://*.nhentai.net/*
// @match       *://*.niyaniya.moe/*
// @match       *://*.panda.chaika.moe/*
// @match       *://*.pururin.io/*
// @match       *://*.pururin.to/*
// @match       *://*.seia.to/*
// @match       *://*.shupogaki.moe/*
// @match       *://*.tsumino.com/*
// ==/UserScript=="
`

module.exports = {
  node: {
    global: false
  },
  entry: path.join(__dirname, "src/main/js/App.tsx"),
  output: {
    filename: "hentag-bookmarker.user.js",
    path: path.resolve(__dirname, "dist")
  },
  resolve: {
    // Add ".ts" and ".tsx" as resolvable extensions.
    modules: [
      path.resolve(__dirname, "src/main/js"),
      path.resolve(__dirname, "src/main/style"),
      "node_modules"
    ],
    extensions: [".ts", ".tsx", ".js", ".jsx", ".css", ".less"]
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          format: {
            comments: /==\/?UserScript|@(name|namespace|match|grant|version|author|description|icon|downloadURL|supportURL|homepageURL|require|noframes)/i
          },
        },
        extractComments: false
      }),
    ],
  },
  watchOptions: {
    aggregateTimeout: 200,
    poll: 1000,
    ignored: /node_modules/
  },
  plugins: [
    // new BundleAnalyzerPlugin(),
    new CleanWebpackPlugin(),
    new webpack.BannerPlugin({banner: userScriptMetadata, raw: true})
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/i,
        exclude: /node_modules/,
        loader: 'esbuild-loader',
        options: {
          loader: 'tsx',
          target: 'es2018'
        }
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          },
          {
            loader: "less-loader", // compiles Less to CSS
            // options: {
            //   lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
            //     javascriptEnabled: true,
            //   }
            // }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [{
          loader: "style-loader" // creates style nodes from JS strings
        }, {
          loader: "css-loader" // translates CSS into CommonJS
        }, {
          loader: "sass-loader" // compiles Sass to CSS
        }]
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {loader: "css-loader", options: {importLoaders: 1}},
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                ident: "postcss",
                plugins: [
                  require("tailwindcss")("./tailwind.config.js"),
                  require("postcss-preset-env")
                ]
              }
            },
          }
        ],
      },
      {
        test: /.*\.(gif|png|jpe?g)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "img/[name].[ext]",
            }
          },
          {
            loader: "image-webpack-loader"
          }
        ]
      }
    ]
  }
};