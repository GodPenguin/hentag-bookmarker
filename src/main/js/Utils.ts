export function normalizeUrl(s: string): string[] {
  try {
    const url = new URL(s);
    const domain = url.hostname.startsWith("www.") ? url.hostname.slice(4) : url.hostname;
    const path = url.pathname.endsWith("/") ? url.pathname.slice(0, -1) : url.pathname;

    const normalizedUrl = `${domain}${path}`;

    if (domain === "e-hentai.org") {
      return [
        normalizedUrl,
        `exhentai.org${path}`
      ];
    } else if (domain === "exhentai.org") {
      return [
        normalizedUrl,
        `e-hentai.org${path}`
      ];
    } else if (domain === "doujin.io") {
      return [
        normalizedUrl,
        `doujin.io${path.split("/").slice(0, 3).join("/")}`
      ];
    } else {
      return [`${domain}${path}`];
    }
  } catch (e) {
    return [s];
  }
}