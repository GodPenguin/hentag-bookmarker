import {BookmarkConflictDto} from "BookmarkService";
import {DEFAULT_MARGIN_BOTTOM, HENTAG_URL} from "globals";
import * as React from "react";
import Link from "Link";

export default function ConflictError(props: { conflictDto: BookmarkConflictDto }): React.JSX.Element {
  return <div style={{marginBottom: DEFAULT_MARGIN_BOTTOM}}>
    <div className="hentag-text-yellow-300" style={{fontSize: "14px"}}>
      {props.conflictDto.message}
    </div>
    <Link url={`${HENTAG_URL}/work/${props.conflictDto.existingWorkId}`} fontSize={"14px"} text={"View Existing"}/>
  </div>
}
