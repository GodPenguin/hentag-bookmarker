import * as React from "react";

export default function Spinner(): React.JSX.Element {
  return <div className={"hentag-flex hentag-items-center hentag-justify-center"}>
    <div className="hentag-inline-flex hentag-items-center hentag-mt-1 hentag-spinner" style={{width: "18px", height: "18px"}}>
      <div className="hentag-bounce1"/>
      <div className="hentag-bounce2"/>
      <div className="hentag-bounce3"/>
    </div>
  </div>
}