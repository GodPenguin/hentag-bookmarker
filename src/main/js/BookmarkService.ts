import {HENTAG_URL} from "globals";

export type ResponseHandler = (response: GM.Response<never>) => void;

export default class BookmarkService {

  static bookmark(dto: CreateBookmarkDto, handleResponse: ResponseHandler, handleError: ResponseHandler, mode?: "replace" | "merge"): void {
    const qp = mode ? `?${mode}` : "";
    GM.xmlHttpRequest({
      method: "POST",
      url: `${HENTAG_URL}/user/api/browser-ext/bookmark${qp}`,
      data: JSON.stringify(dto),
      onload: handleResponse,
      onerror: handleError
    })
  }

  static refreshCurrentBookmarks(handleResponse: ResponseHandler, handleError: ResponseHandler): void {
    GM.xmlHttpRequest({
      method: "GET",
      url: `${HENTAG_URL}/user/api/browser-ext/bookmark`,
      onload: handleResponse,
      onerror: handleError
    })
  }

  static saveBookmarksLocally(bookmarkState: BookmarkState): Promise<void> {
    return GM.setValue(BOOKMARK_STORAGE_KEY, JSON.stringify(bookmarkState)).catch((e) => console.error(e));
  }

  static readLocalBookmarks(): Promise<BookmarkState> {
    return GM.getValue(BOOKMARK_STORAGE_KEY)
      .then((res?: string) => {
        return res ? JSON.parse(res) as BookmarkState : EMPTY_BOOKMARK_STATE;
      }).catch((err) => {
        console.error(err);
        return EMPTY_BOOKMARK_STATE
      });
  }
}

export const EMPTY_BOOKMARK_STATE: BookmarkState = {bookmarks: {} as Record<string, string[]>, lastSync: 0};

export type BookmarkDto = {
  workId: string;
  url: string;
}

export type BookmarkState = {
  bookmarks: Record<string, string[]>;
  lastSync: number;
}

export type CreateBookmarkDto = {
  url: string;
  payload?: string;
  requestId?: string;
}

export type NeedPayloadDto = {
  requestId: string;
}

export type BookmarkConflictDto = {
  message: string;
  existingWorkId: string;
}

export const BOOKMARK_STORAGE_KEY = "bookmarkState";

export function makeResponseError(error: string): GM.Response<never> {
  return {
    context: undefined, finalUrl: "", readyState: 4, response: undefined, responseHeaders: "", responseText: error, responseXML: false, status: 0, statusText: ""
  }
}