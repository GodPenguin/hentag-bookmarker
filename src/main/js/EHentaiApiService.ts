import {makeResponseError, ResponseHandler} from "BookmarkService";

export default class EHentaiApiService {

  private static API_URL = "https://api.e-hentai.org/api.php";

  private static extractGalleryId(url: string): EHentaiGalleryId | undefined {
    try {
      const u = new URL(url);
      const parts = u.pathname.split("/").filter(x => x.length > 0);
      return {
        id: parts[1],
        token: parts[2]
      };
    } catch (e) {
      return undefined;
    }
  }

  static fetch(url: string, handleResponse: ResponseHandler, handleError: ResponseHandler): void {
    const galleryId = this.extractGalleryId(url);

    if (galleryId) {
      const apiReqDto: EHentaiApiRequest = {
        gidlist: [[galleryId.id, galleryId.token]],
        method: "gdata",
        namespace: 1
      };

      GM.xmlHttpRequest({
        method: "POST",
        url: this.API_URL,
        data: JSON.stringify(apiReqDto),
        onload: handleResponse,
        onerror: handleError
      })
    } else {
      handleError(makeResponseError("Cannot bookmark this page. Try the work's overview page"))
    }
  }
}

type EHentaiGalleryId = {
  id: string;
  token: string;
}

type EHentaiApiRequest = {
  method: string;
  gidlist: string[][];
  namespace: number;
}