import * as React from "react";
import {DEFAULT_MARGIN_BOTTOM} from "globals";

type LinkProps = {
  url: string;
  fontSize?: string;
  children?: React.ReactNode
  text?: string;
  className?: string;
  noMarginBottom?: boolean;
}

export default function Link(props: LinkProps): React.JSX.Element {
  return <a className={`hentag-cursor-pointer hentag-font-medium ${props.className || ""}`}
            href={props.url}
            target="_blank"
            rel="noopener noreferrer"
            style={{fontSize: props.fontSize, marginBottom: props.noMarginBottom ? 0 : DEFAULT_MARGIN_BOTTOM}}>
    {props.children || props.text || props.url}
  </a>
}