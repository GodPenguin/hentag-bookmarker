import * as React from "react";
import {useEffect, useRef, useState} from "react";
import BookmarkService, {BookmarkDto, BookmarkState, NeedPayloadDto, ResponseHandler} from "BookmarkService";
import NotLoggedInView from "NotLoggedInView";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar, faSync} from "@fortawesome/free-solid-svg-icons";
import {faStar as faStarOutline} from "@fortawesome/free-regular-svg-icons"
import Button from "Button";
import {normalizeUrl} from "Utils";
import Link from "Link";
import {DEFAULT_MARGIN_BOTTOM, HENTAG_URL} from "globals";
import EHentaiApiService from "EHentaiApiService";
import MangadexApiService from "MangadexApiService";
import ConflictError from "ConflictError";

export default function BookmarkModalView(props: { bookmarkState: BookmarkState, setBookmarkState: (bs: BookmarkState) => void }): React.JSX.Element {
  const mounted: React.MutableRefObject<boolean> = useRef(false);
  useEffect(() => {
    mounted.current = true;

    return () => {
      mounted.current = false
    };
  }, []);

  const [isSaving, setIsSaving] = useState<boolean>(false);
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false);
  const [showReplaceMergeOptions, setShowReplaceMergeOptions] = useState<boolean>(false);
  const [error, setError] = useState<string | React.JSX.Element>("");

  function witErrorHandling(setLoadingStatus: (loading: boolean) => void, successHandler: ResponseHandler): (resp: GM.Response<never>) => void {
    return (resp: GM.Response<never>) => {
      if (resp.status === 404 && mounted.current) {
        setLoadingStatus(false);
        setError(<NotLoggedInView/>);
      } else if (resp.status === 409 && mounted.current) {
        setError(<ConflictError conflictDto={JSON.parse(resp.responseText)}/>)
        setLoadingStatus(false);
        setShowReplaceMergeOptions(true)
      } else if (resp.status >= 400) {
        setError(ErrorMessage(resp.responseText || "Error bookmarking. Try the work's index page"))
        setLoadingStatus(false);
      } else {
        successHandler(resp)
      }
    }
  }

  function onRequestError(setLoadingStatus: (loading: boolean) => void, defaultMessage: string): (resp: GM.Response<never>) => void {
    return (resp) => {
      console.error(resp)
      if (mounted.current) {
        setError(ErrorMessage(resp.responseText || defaultMessage))
        setLoadingStatus(false)
      }
    }
  }

  const resyncBookmarks = () => {
    setError("")
    setShowReplaceMergeOptions(false);
    setIsRefreshing(true);
    BookmarkService.refreshCurrentBookmarks(
      witErrorHandling(setIsRefreshing, (resp) => {
        setIsRefreshing(false);
        if (resp.responseText) {
          const refreshed: BookmarkState = {bookmarks: JSON.parse(resp.responseText), lastSync: Date.now()}
          props.setBookmarkState(refreshed)
          return BookmarkService.saveBookmarksLocally(refreshed)
        }
      }),
      onRequestError(setIsRefreshing, "Error resyncing")
    )
  };

  let bookmarkButton;
  const normalizedTabUrls: string[] = normalizeUrl(window.location.href).flatMap(x => props.bookmarkState.bookmarks[x] || []);
  const existingBookmarks = normalizedTabUrls
    .map((workId, ndx) => <Link key={workId} url={`${HENTAG_URL}/work/${workId}`} text={`View Existing${ndx ? ` #${ndx + 1}` : ""}`} fontSize={"14px"}/>)

  const updateLocalBookmarks = (bookmarkDto: BookmarkDto) => {
    const mergedEntry = {[bookmarkDto.url]: (props.bookmarkState.bookmarks[bookmarkDto.url] || []).concat(bookmarkDto.workId)};
    const xs = Object.assign({}, props.bookmarkState.bookmarks, mergedEntry);
    const updatedState = {
      bookmarks: xs,
      lastSync: Date.now()
    };
    if (mounted.current) {
      setIsSaving(false);
      props.setBookmarkState(updatedState);
    }
    return BookmarkService.saveBookmarksLocally(updatedState);
  }

  if (existingBookmarks.length) {
    bookmarkButton = <Button onClick={() => null} className="hentag-bg-pink-800 hover:hentag-bg-pink-800" disabled disableClassOverride="hentag-cursor-default">
      <FontAwesomeIcon id="hentag-star" style={{marginRight: "6px"}} icon={faStar} className="hentag-text-yellow-400"/>Bookmarked
    </Button>
  } else {
    const onBookmark = () => {
      setIsSaving(true);
      setError("");
      const url = window.location.href;
      return BookmarkService
        .bookmark({url: url}, witErrorHandling(setIsSaving, (resp) => {
            const responseDto: BookmarkDto | NeedPayloadDto = JSON.parse(resp.responseText);
            if (resp.status === 202) {
              const requestId = (responseDto as NeedPayloadDto).requestId;

              let domain;
              try {
                const u = new URL(url);
                domain = u.hostname
              } catch (e) {
                console.error(e)
              }

              if (domain === "e-hentai.org" || domain === "exhentai.org") {
                EHentaiApiService.fetch(url,
                  witErrorHandling(setIsSaving, (resp) => {
                    BookmarkService.bookmark(
                      {url: url, payload: resp.responseText, requestId: requestId},
                      witErrorHandling(setIsSaving, (resp) => updateLocalBookmarks(JSON.parse(resp.responseText))),
                      onRequestError(setIsSaving, "Error e-hentai parsing")
                    )
                  }),
                  onRequestError(setIsSaving, "Error with E-hentai API")
                );
              } else if (domain === "mangadex.org") {
                MangadexApiService.fetch(url,
                  witErrorHandling(setIsSaving, (resp) => {
                    BookmarkService.bookmark(
                      {url: url, payload: resp.responseText, requestId: requestId},
                      witErrorHandling(setIsSaving, (resp) => updateLocalBookmarks(JSON.parse(resp.responseText))),
                      onRequestError(setIsSaving, "Error e-hentai parsing")
                    )
                  }),
                  onRequestError(setIsSaving, "Error with Mangadex API")
                );
              } else {
                BookmarkService
                  .bookmark(
                    {url: url, payload: document.documentElement.innerHTML, requestId: requestId},
                    witErrorHandling(setIsSaving, (resp) => updateLocalBookmarks(JSON.parse(resp.responseText))),
                    onRequestError(setIsSaving, "Error bookmarking with HTML")
                  );
              }
            } else {
              return updateLocalBookmarks(responseDto as BookmarkDto);
            }
          }),
          onRequestError(setIsSaving, "Error bookmarking")
        );
    };

    bookmarkButton = <Button onClick={onBookmark}
                             className="hentag-bg-purple-700 hover:hentag-bg-purple-600"
                             performingAction={isSaving}
                             disabled={showReplaceMergeOptions}>
      <FontAwesomeIcon id="hentag-star" style={{marginRight: "6px"}} icon={faStarOutline}/>Bookmark
    </Button>
  }

  const handleReplaceOrMerge = (url: string, mode: "replace" | "merge") => {
    return () => {
      setIsSaving(true);
      BookmarkService
        .bookmark({url: url},
          witErrorHandling(setIsSaving, (resp) => {
            setShowReplaceMergeOptions(false);
            setError("");
            return updateLocalBookmarks(JSON.parse(resp.responseText) as BookmarkDto);
          }),
          onRequestError(setIsSaving, `Error with ${mode}`),
          mode
        )
    };
  };

  let replaceMergeButtons;
  if (showReplaceMergeOptions) {
    replaceMergeButtons = <div className="hentag-grid hentag-grid-cols-2 hentag-gap-2" style={{marginBottom: DEFAULT_MARGIN_BOTTOM}}>
      <Button className="hentag-bg-yellow-700 hover:hentag-bg-yellow-600 mt-1" onClick={handleReplaceOrMerge(window.location.href, "merge")}>Merge</Button>
      <Button className="hentag-bg-yellow-700 hover:hentag-bg-yellow-600 mt-1" onClick={handleReplaceOrMerge(window.location.href, "replace")}>Replace</Button>
    </div>;
  }

  let prettyDate;
  if (props.bookmarkState.lastSync) {
    const date = new Date(props.bookmarkState.lastSync);
    prettyDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
  } else {
    prettyDate = "--";
  }

  return <div className="hentag-flex hentag-flex-col hentag-justify-between">
    {bookmarkButton}
    {error}
    {replaceMergeButtons}
    {existingBookmarks}
    <Button key={"refresh"} className="hentag-bg-green-700 hover:hentag-bg-green-600" onClick={resyncBookmarks} noBottomMargin>
      <FontAwesomeIcon className={`${isRefreshing ? "hentag-animate-spin" : ""}`} style={{marginRight: "6px"}} icon={faSync}/>Resync
    </Button>
    <div>
      <a href="https://hentag.com/works" target="_blank" rel="noopener noreferrer" className="hentag-italic" style={{fontSize: "12px"}}>Synced: {prettyDate}</a>
    </div>
  </div>
}

const ErrorMessage = (msg: string) => {
  return <div className="hentag-text-red-400" style={{marginBottom: DEFAULT_MARGIN_BOTTOM, fontSize: "14px"}}>
    {msg}
  </div>
}