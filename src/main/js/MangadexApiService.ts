import {makeResponseError, ResponseHandler} from "BookmarkService";

export default class MangadexApiService {

  private static API_URL = "https://api.mangadex.org/v2/";

  private static extractMangaId(url: string): string | undefined {
    try {
      const u = new URL(url);
      const parts = u.pathname.split("/").filter(x => x.length > 0);
      return parts[1];
    } catch (e) {
      return undefined;
    }
  }

  static fetch(url: string, handleResponse: ResponseHandler, handleError: ResponseHandler): void {
    const mangaId = this.extractMangaId(url);

    if (mangaId) {

      GM.xmlHttpRequest({
        method: "GET",
        url: `${this.API_URL}/manga/${mangaId}`,
        onload: handleResponse,
        onerror: handleError
      })
    } else {
      handleError(makeResponseError("Cannot bookmark this page. Try the work's overview page"));
    }
  }
}