import * as React from "react";
import Spinner from "Spinner";
import {DEFAULT_MARGIN_BOTTOM} from "globals";

type ButtonProps = {
  onClick: (event: React.MouseEvent) => void;
  performingAction?: boolean;
  className: string
  children?: React.ReactNode;
  title?: string;
  disabled?: boolean;
  disableClassOverride?: string;
  noBottomMargin?: boolean;
}

export default function Button(props: ButtonProps): React.JSX.Element {

  const maybeDisabled = props.disabled ? (props.disableClassOverride || `hentag-cursor-not-allowed hentag-bg-opacity-50"`) : "hentag-cursor-pointer";
  return <button
    className={`${maybeDisabled} hentag-rounded hentag-text-gray-300 hentag-font-medium ${props.className} hentag-flex hentag-items-center hentag-justify-center`}
    style={{fontSize: "18px", paddingTop: "4px", paddingBottom: "4px", marginBottom: props.noBottomMargin ? 0 : DEFAULT_MARGIN_BOTTOM}}
    onClick={props.onClick}
    title={props.title}
    disabled={props.disabled || props.performingAction}>
    {props.performingAction ? <Spinner/> : props.children}
  </button>
}
