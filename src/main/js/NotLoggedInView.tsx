import Link from "Link";
import {DEFAULT_MARGIN_BOTTOM, HENTAG_URL} from "globals";
import * as React from "react";

export default function NotLoggedInView(): React.JSX.Element {
  return <div style={{marginBottom: DEFAULT_MARGIN_BOTTOM}}>
    <Link url={HENTAG_URL} text={"Hentag Log In"} fontSize={"16px"}/>
    <div style={{marginTop: "8px", fontSize: "12px"}}>{"You'll need to first log in to your account to use the extension"}</div>
  </div>;
}