import * as React from "react";
import {useEffect, useRef, useState} from "react";
import "App.css";
import BookmarkModalView from "BookmarkModalView";
import BookmarkService, {BookmarkState, EMPTY_BOOKMARK_STATE} from "BookmarkService";
import {normalizeUrl} from "Utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {createRoot} from "react-dom/client";

const REFRESH_BOOKMARK_AFTER_MS = 2 * 60 * 60 * 1000; // 2 hours

function App(): React.JSX.Element {
  const wrapperRef = useRef<HTMLDivElement>(null);

  const mounted: React.MutableRefObject<boolean> = useRef(false);
  useEffect(() => {
    mounted.current = true;

    return () => {
      mounted.current = false;
    };
  }, []);

  const [showModal, setShowModal] = useState<boolean>(false);
  const [bookmarkState, setBookmarkState] = useState<BookmarkState>(EMPTY_BOOKMARK_STATE);

  useEffect(() => {
    BookmarkService.readLocalBookmarks().then((bs) => {
      if (bs && bs.lastSync + REFRESH_BOOKMARK_AFTER_MS < Date.now()) {
        BookmarkService.refreshCurrentBookmarks((resp) => {
          if (mounted.current && resp.responseText) {
            const refreshed: BookmarkState = {bookmarks: JSON.parse(resp.responseText), lastSync: Date.now()};
            setBookmarkState(refreshed);
            return BookmarkService.saveBookmarksLocally(refreshed);
          }
        }, (resp) => {
          console.error(resp);
        });
      } else {
        if (mounted.current) setBookmarkState(bs);
      }
    }).catch((err) => {
      console.error(err);
    });
  }, []);

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleClosingMenuOnOutsideClick = (event: any) => {
      if (wrapperRef && wrapperRef.current && !wrapperRef.current.contains(event.target) && event.target.id !== "hentag-star") {
        setShowModal(false);
      }
    };

    document.addEventListener('mousedown', handleClosingMenuOnOutsideClick);

    return () => {
      document.removeEventListener('mousedown', handleClosingMenuOnOutsideClick);
    };
  }, []);

  const alreadyBookmarked = Boolean(normalizeUrl(window.location.href).flatMap(x => bookmarkState.bookmarks[x] || []).length);

  return <>
    <div id="hentag-bookmark-icon"
         key="icon"
         onClick={() => setShowModal(true)}
         className={`hentag-fixed hentag-top-0 hentag-right-0 hentag-cursor-pointer ${showModal ? "hentag-hidden" : ""}`}
         style={{zIndex: 9999}}>
      <img className="hentag-opacity-80 hentag-fixed hentag-top-0 hentag-right-0"
           alt="Hentag Bookmarker"
           src="https://hentag.com/static/icon/favicon-32x32.png"/>
      {alreadyBookmarked && !showModal
        ? <FontAwesomeIcon icon={faStar} className="hentag-text-yellow-400" style={{position: "absolute", top: 0, right: 0, fontSize: "10px"}}/>
        : null}
    </div>
    <div ref={wrapperRef}
         key="modal"
         className={`hentag-fixed hentag-top-0 hentag-right-0 hentag-bg-gray-900 hentag-bg-opacity-90 hentag-rounded hentag-shadow-lg ${showModal ? "" : "hentag-invisible hentag-overflow-hidden"}`}
         style={{width: showModal ? "200px" : 0, height: showModal ? "auto" : 0, zIndex: 9999}}>
      <div style={{padding: "8px 8px 2px 8px"}}>
        <BookmarkModalView bookmarkState={bookmarkState} setBookmarkState={setBookmarkState}/>
        <span className="hentag-cursor-pointer hentag-text-red-300 hover:hentag-text-red-200 hentag-font-medium" onClick={() => setShowModal(false)}
              style={{fontSize: "12px"}}>[close]</span>
      </div>
    </div>
  </>;
}

const entry = document.createElement("div");
entry.setAttribute("id", "hentag-bookmarker");
entry.setAttribute("class", "hentag-fixed hentag-top-0 hentag-right-0 hentag-text-gray-300 hentag-p-0 hentag-m-0 hentag-max-w-[200px]");
entry.setAttribute("style", "z-index:9999;all:revert;");
document.body.prepend(entry);

const container = document.getElementById("hentag-bookmarker");
// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(container!);
root.render(<App/>);